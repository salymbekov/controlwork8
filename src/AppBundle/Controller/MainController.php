<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Application;
use AppBundle\Entity\Reader;
use AppBundle\Form\ApplicationType;
use AppBundle\Form\regType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class MainController extends Controller
{
    /**
     * @Route("/{id}", requirements={"id" = "\d+"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($id = null)
    {
        $categories = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->findAll();
        $categoryForShow = '';
        if($id !== null){
            $categoryForShow = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Category')
                ->find($id);
        }
        return $this->render('@App/Main/index.html.twig', [
            'categories' => $categories,
            'categoryForShow' => $categoryForShow
        ]);
    }

    /**
     * @Route("/registration")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        $reader = new Reader();

        $form = $this->createForm(regType::class, $reader, [
            'method' => 'POST'
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reader = $form->getData();
            $readerTicket = $reader->getPassport().rand(1,100000);
            $reader->setReaderTicket($readerTicket);

            $em = $this->getDoctrine()->getManager();
            $em->persist($reader);
            $em->flush();

            return $this->render("@App/Main/registerSuccess.html.twig", [
                'ticket' => $readerTicket
            ]);
        }

        return $this->render('@App/Main/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/application/{id}", requirements={"id" = "\d+"})
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ApplicationAction($id, Request $request)
    {
        /**
         * @var Application
         */
        $application = new Application();
        $form = $this->createForm(ApplicationType::class, $application, [
            'method' => 'POST'
        ]);
        $book = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Book')
            ->find($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $application = $form->getData();
            $application->setBook($book);
            $em = $this->getDoctrine()->getManager();
            $em->persist($application);
            $em->flush();

            return $this->redirectToRoute('app_main_index');
        }
        return $this->render('@App/Main/application.html.twig', [
            'form' => $form->createView(),
            'book' => $book
        ]);
    }

    /**
     * @Route("/{_locale}/", requirements = {"_locale" : "en|ru|"})
     */
    public function LocaleAction(){
        return $this->redirectToRoute('app_main_index');
    }
}
