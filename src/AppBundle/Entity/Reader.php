<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reader
 *
 * @ORM\Table(name="reader")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReaderRepository")
 */
class Reader
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="passport", type="text")
     */
    private $passport;

    /**
     * @var string
     *
     * @ORM\Column(name="readerTicket", type="text")
     *
     *
     */
    private $readerTicket;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Application", mappedBy="ticket")
     */
    private $applications;

    public function __construct()
    {
        $this->applications = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Reader
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Reader
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set passport
     *
     * @param string $passport
     *
     * @return Reader
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;

        return $this;
    }

    /**
     * Get passport
     *
     * @return string
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param string $readerTicket
     * @return Reader
     */
    public function setReaderTicket(string $readerTicket)
    {
        $this->readerTicket = $readerTicket;
        return $this;
    }

    /**
     * @return string
     */
    public function getReaderTicket()
    {
        return $this->readerTicket;
    }

    /**
     * @return ArrayCollection
     */
    public function getApplications(): ArrayCollection
    {
        return $this->applications;
    }

    public function addApplications(Application $application){
        $this->applications->add($application);
    }
}

