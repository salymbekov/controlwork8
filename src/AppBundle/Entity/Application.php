<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Application
 *
 * @ORM\Table(name="application")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicationRepository")
 */
class Application
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="ticket", type="text")
     *
     */
    private $ticket;

    /**
     * @var string
     *
     * @ORM\Column(name="realReturnDate", type="datetime", nullable=true)
     */
    private $realReturnDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="return_date", type="datetime")
     */
    private $returnDate;

    /**
     * @var Book
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Book", inversedBy="applications")
     */
    private $book;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ticket
     *
     * @param string $ticket
     *
     * @return Application
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * Get ticket
     *
     * @return string
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Set returnDate
     *
     * @param \DateTime $returnDate
     *
     * @return Application
     */
    public function setReturnDate($returnDate)
    {
        $this->returnDate = $returnDate;

        return $this;
    }

    /**
     * Get returnDate
     *
     * @return \DateTime
     */
    public function getReturnDate()
    {
        return $this->returnDate;
    }

    /**
     * @param Book $book
     * @return Application
     */
    public function setBook(Book $book)
    {
        $this->book = $book;
        return $this;
    }

    /**
     * @return Book
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param string $status
     * @return Application
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $realReturnDate
     * @return Application
     */
    public function setRealReturnDate(string $realReturnDate): Application
    {
        $this->realReturnDate = $realReturnDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getRealReturnDate(): string
    {
        return $this->realReturnDate;
    }
}

