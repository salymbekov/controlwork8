<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class regType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Name', TextType::class, [
                'label' => 'ФИО'
            ])
            ->add('address', TextType::class, [
                'label' => 'Адрес'
            ])
            ->add('passport', TextType::class, [
                'label' => 'Пасспорт'
            ])
        ->add('save', SubmitType::class,  [
            "label" => 'Получить читательский билет'
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundlereg_type';
    }
}
