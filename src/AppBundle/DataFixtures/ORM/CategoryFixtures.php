<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class CategoryFixtures extends Fixture
{
    const HORROR = 'horror';
    const COMEDY = 'comedy';
    const FANTASY = 'fantasy';
    const RANOBE = 'ranobe';

    public function load(ObjectManager $manager)
    {

        $category = new Category();
        $category->setName('horror');
        $manager->persist($category);

        $manager->flush();
        $category2 = new Category();
        $category2->setName('comedy');
        $manager->persist($category2);

        $manager->flush();
        $category3 = new Category();
        $category3->setName('fantasy');
        $manager->persist($category3);

        $manager->flush();
        $category4 = new Category();
        $category4->setName('ranobe');
        $manager->persist($category4);

        $manager->flush();


        $this->addReference(self::HORROR, $category);
        $this->addReference(self::COMEDY, $category2);
        $this->addReference(self::FANTASY, $category3);
        $this->addReference(self::RANOBE, $category4);

    }
}
