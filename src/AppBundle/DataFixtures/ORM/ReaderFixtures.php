<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Reader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class ReaderFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $reader = new Reader();
        $passport = 'AN458756';
        $readerTicket = $passport . rand(1, 100000);
        $reader
            ->setName('Вася Пупкин')
            ->setAddress('Ney-York brodway st. 45')
            ->setPassport($passport)
            ->setReaderTicket($readerTicket);

        $manager->persist($reader);

        $manager->flush();
    }


    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     *
     */
    function getDependencies()
    {
        // TODO: Implement getDependencies() method.
    }
}
