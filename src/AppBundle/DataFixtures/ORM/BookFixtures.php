<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


class BookFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        /** @var Category $category1 */
        $category1 = $this->getReference(CategoryFixtures::HORROR);
        /** @var Category $category2 */
        $category2 = $this->getReference(CategoryFixtures::FANTASY);
        /** @var Category $category3 */
        $category3 = $this->getReference(CategoryFixtures::COMEDY);
        /** @var Category $category4 */
        $category4 = $this->getReference(CategoryFixtures::RANOBE);
        for ($i = 0; $i < 5; $i++) {
            $book = new Book();
            $book
                ->setName('harry' . $i)
                ->setImage('book.jpg')
                ->setAuthor('joane rowling')
                ->addCategory($category1);
            $manager->persist($book);
            $manager->flush();
        }
        for ($i = 0; $i < 5; $i++) {
            $book = new Book();
            $book
                ->setName('sumrak' . $i)
                ->setImage('book.jpg')
                ->setAuthor('joane rowling')
                ->addCategory($category2);
            $manager->persist($book);
            $manager->flush();
        }
        for ($i = 0; $i < 5; $i++) {
            $book = new Book();
            $book
                ->setName('black panther' . $i)
                ->setImage('book.jpg')
                ->setAuthor('joane rowling')
                ->addCategory($category3);
            $manager->persist($book);
            $manager->flush();
        }
        for ($i = 0; $i < 5; $i++) {
            $book = new Book();
            $book
                ->setName('batman' . $i)
                ->setImage('book.jpg')
                ->setAuthor('joane rowling')
                ->addCategory($category4);
            $manager->persist($book);
            $manager->flush();
        }

    }

    public function getDependencies()
    {
        return array(
            CategoryFixtures::class,
        );
    }
}
